#ifndef PR_SCALAR_MESON_PHOTON_H
#define PR_SCALAR_MESON_PHOTON_H

#include "TTree.h"
#include "TMath.h"

class CsPrScalarMesonPhoton : public VProcess {

private:
    // TODO: move constants to library.
    const Double_t Gamma = 0.30e-3;     // H->2gamma cross-section for a0
    // TODO: change Gamma to function Gamma(Int_t pdg)
    const Double_t m_e = 0.511;         // Mass of the e-
    const Double_t m_H = 980.0;         // Mass of the meson a0

    Double_t FormfactorSqrd(Double_t s);

public:
    Double_t PDF(Int_t nDim, Double_t* args);
    virtual Double_t Density(Int_t nDim, Double_t* args) override {return PDF(nDim, args);}
    
    Double_t ReversedPDF(Int_t nDim, Double_t* args);
    Double_t PDFt(Double_t t);
    Int_t Initialize();
    void PrintInfo();

    CsPrScalarMesonPhoton();

};


#endif // PR_SCALAR_MESON_PHOTON_H

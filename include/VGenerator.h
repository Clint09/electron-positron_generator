#ifndef VGENERATOR_H
#define VGENERATOR_H

#include <string>
#include <vector>
#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include "VProcess.h"


class VGenerator {

protected:
    UInt_t seed = 1984;
    TString processName = "scalar_meson_photon";
    //UInt_t nEvents = 1e6;
    VProcess* Process = NULL;
    Double_t integral;
    // TOASK: is it ok that Process is protected?
    // TOASK: is it ok to have defaults here?
    //        Shouldnt they be in the constructor?
    virtual Double_t Integral() =0;
    // TODO: undestand why Integral() is protected, ok?
    TLorentzVector* AnglesToMomenta( Double_t* angles,
                                     TLorentzVector* momenta );


public:
    //Int_t SetNEvents(UInt_t N){ nEvents = N; return 0; }; // TODO: Deprecated?
    // TODO2: or not depricated? It can be useful to calculate
    //        the number of bins in histogram for Neumann method
    Int_t SetProcess(TString prName);//{processName = prName; return 0;};
    Int_t SetSeed(UInt_t s) { seed = s; return 0; };
    Int_t SetBeamEnergy(Double_t E) {Process->SetBeamEnergy(E); return 0;};

    //UInt_t GetNEvents() { return nEvents; };
    TString GetProcessName() { return processName; };
    Double_t GetProcessEnergy() { return Process->BeamEnergy(); }
    Double_t GetIntegral() { return integral; }
    virtual Int_t Initialize() =0;
    //virtual Int_t Finalize() =0; // initialize integral
    virtual Int_t MakeEvent(TLorentzVector *momenta) =0;
    Int_t FillTree(TTree* tree, UInt_t N);

};
// TODO: to make MakeEvent private (or protected) ! Move all checks to FillTree!

#endif // VGENERATOR_H

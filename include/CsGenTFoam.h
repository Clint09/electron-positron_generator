#ifndef CSGENTFOAM_H
#define CSGENTFOAM_H

//#include <string>
#include <vector>
#include "TTree.h"
//#include "TString.h"
#include "TLorentzVector.h"
#include "TFoam.h"

#include "VGenerator.h"


class CsGenTFoam : public VGenerator {

private:
    const UInt_t dim = 2;                   // Dimension of TFoam event
    TFoam *Foam = NULL;                     // TFoam instance
    UInt_t nCells = 1000;                   // TFoam cells

    Double_t Integral();
    

public:
    CsGenTFoam();
    Int_t Initialize();
    //Int_t Finalize();
    Int_t MakeEvent(TLorentzVector* momenta);
    Int_t SetnCells (UInt_t nCells) { this->nCells = nCells; return 0; }
    UInt_t GetnCells () { return nCells; }
};

#endif // CSGENTFOAM_H

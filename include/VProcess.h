#ifndef VPROCESS_H
#define VPROCESS_H

#include <vector>
#include "TTree.h"
#include "TMath.h"
#include "TFoamIntegrand.h"

#include "CsParticle.h"


class VProcess : public TFoamIntegrand {

public:
    const Double_t alpha = 1.0/137.0;     // Fine-structure const

protected:
    Double_t E = 1000;                       // Energy of the in-particle
    std::vector<CsParticle> outParticles; // Initial info about out-state particles
    std::vector<CsParticle> inParticles;  // Initial info about in-state particles
    // TOASK: is it ok to have protected members?
    //        The child changing the particles ditectly during initialization
    Double_t major = 0;   // maximum of PDF for Neumann method
    Double_t t_limit[2] = {0};

public:
    Int_t SetBeamEnergy (Double_t E) { this->E = E;   return 0; }

    Double_t  BeamEnergy()  { return this->E;             }
    UInt_t    NOut()        { return outParticles.size(); }
    UInt_t    NIn()         { return inParticles.size();  }
    Double_t  Major()       { return major;               }
    Double_t  T_min()       { return t_limit[0];          }
    Double_t  T_max()       { return t_limit[1];          }
    // TODO: I guess these T_min, T_max and other things specific to generating methods
    // must be in the class of method as members or another class
    const std::vector<CsParticle>& OutParticles() { return outParticles; }
    const std::vector<CsParticle>& InParticles()  { return inParticles;  }
    CsParticle& OutParticle(UInt_t i) { return outParticles.at(i); }
    CsParticle& InParticle (UInt_t i) { return inParticles.at(i);  }
    // TOASK: when I use "const CsParticle ..." it doesn't work. Why?

    virtual Double_t PDF(Int_t nDim, Double_t* args) =0;
    virtual Double_t ReversedPDF(Int_t nDim, Double_t* args) =0;
    virtual Double_t PDFt(Double_t t) =0;
    virtual Int_t Initialize() =0;
    virtual void     PrintInfo() =0;
};



#endif // VPROCESS_H

#ifndef CSPARTICLE_H
#define CSPARTICLE_H

#include <iostream>
#include "TTree.h"
#include "TString.h"
#include "TVector3.h"
#include "TLorentzVector.h"

class CsParticle {
private:
    Int_t pdg;
    TString name;
    Double_t mass;
    TLorentzVector momentum;

public:
    //CsParticle(Int_t pdg);
    //CsParticle(Int_t pdg, TVector3 momentum);
    CsParticle(TString name = "", Double_t mass = 0.0);
    CsParticle(TString name, Double_t mass, TLorentzVector momentum);

    Int_t      Pdg()    { return this->pdg;  }
    TString    Name()   { return this->name; }
    Double_t   Mass()   { return this->mass; }
    Double_t   Energy() { return this->momentum.E(); }
    //const TVector3& Momentum3() { return this->momentum.Vect(); }
    const TLorentzVector& Momentum() { return this->momentum; }
    // TOASK: from the ROOT reference:
    // "TLorentzVector is a legacy class. It is slower and worse for
    // serialization than the recommended superior ROOT::Math::LorentzVector"
    // Should I use Math:: instead?
};

#endif // CSPARTICLE_H

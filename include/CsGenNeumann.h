#ifndef CSGENNEUMANN_H
#define CSGENNEUMANN_H

#include "TTree.h"
#include "TRandom3.h"
#include "TH1.h"

#include "VGenerator.h"


class CsGenNeumann : public VGenerator {

private:
    const UInt_t dim = 2; // dimension of generated vector (NDF)
    TRandom* PseRan = NULL;
    //Double_t major = 0;  // maximum of PDF for Neumann method
    UInt_t accepted = 0;
    UInt_t events = 0;
    //TH1D* histogram;
    UInt_t bins = 1000;

    Double_t MakeT();
    Double_t Integral();

public:
    CsGenNeumann();
    Int_t Initialize();
    Int_t MakeEvent(TLorentzVector* momenta);
    Int_t Finalize();
    // TOASK: shouldn't I specify the inherited API here?

};

#endif // CSGENNEUMANN_H


#include <exception>
#include <iostream>

#include "CsGenNeumann.h"


Double_t CsGenNeumann::MakeT() {
    Double_t t, f, p;
    do {
        t = PseRan->Rndm() * ( Process->T_max() - Process->T_min() )
          + Process->T_min();
        f = PseRan->Rndm() * Process->Major();
        events++;
        p = Process->PDFt(t);
        if (accepted < 10)
            std::cout << "t = " << t << ", f = " << f << ", pdf(t) = " << p << std::endl;
    } while (p < f);
    if (accepted < 10)
        std::cout << "\t ACCEPTED" << std::endl;
    accepted++;
    //histogram->Fill(t);
    return t;
}


Int_t CsGenNeumann::Initialize() {

    if (Process == NULL) {
        throw std::exception();
        std::cerr << "\tERROR! The PROCESS is not set!"
                  << std::endl;
        return 1;
    }
    if (Process->InParticles().size() != 2 ||
        Process->OutParticles().size() != 2) {
        throw std::exception();
        std::cerr << "\tERROR! This generator works only with 2->2 process!"
                  << std::endl;
        return 1;
    }
    Process->Initialize();
    //histogram = new TH1D("h1", "h1 title",
    //                     bins, Process->T_min(), Process->T_max() );
    events = 0;
    accepted = 0;
    PseRan = new TRandom3();
    std::cout << "The generator is initialized." << std::endl;

    return 0;
}


Int_t CsGenNeumann::Finalize() {
    return 0;
}


Int_t CsGenNeumann::MakeEvent(TLorentzVector* momenta) {

    const Double_t E      = Process->BeamEnergy();           // Energy of the beam
    const Double_t M_out0 = Process->OutParticle(0).Mass();  // Mass of the meson a0
    const Double_t E_out0 = Process->OutParticle(0).Momentum().E();
    const Double_t p_e    = Process->InParticle(0).Momentum().Z();
    const Double_t p_H    = TMath::Sqrt( E_out0*E_out0 - M_out0*M_out0 );
    const Double_t p_H2   = E_out0*E_out0 - M_out0*M_out0;

    Double_t t = MakeT();
    Double_t theta = TMath::ACos( (p_H2 + p_e*p_e - (E-E_out0)*(E-E_out0) + t )
                   / (2*p_e*p_H) );
    Double_t phi = PseRan->Rndm() * 2 * TMath::Pi();

    momenta[0].SetXYZM( p_H * TMath::Sin(theta) * TMath::Cos(phi)
                     ,  p_H * TMath::Sin(theta) * TMath::Sin(phi)
                     ,  p_H * TMath::Cos(theta)
                     ,  M_out0);
    momenta[1].SetXYZT( - momenta[0].X()
                     ,  - momenta[0].Y()
                     ,  - momenta[0].Z()
                     ,  2*E - momenta[0].T() );

    return 0;
}


Double_t CsGenNeumann::Integral() {

    // from ratio (accapted / all points )
    Double_t scale_x = Process->T_max() - Process->T_min();
    Double_t scale_y = Process->Major();
    Double_t ratio = ( (double)(accepted) ) / events;
    Double_t integral_points = scale_x * scale_y * ratio;

    // from histogram
    /*
    Double_t t0 = ( ( Process->T_max() - Process->T_min() ) / bins ) / 2
                + Process->T_min();  // the point between 0th and 1st bin;
    long binSum = 0;
    long maxBin = 0;
    for (UInt_t i = 0; i < bins; ++i) {
        long binContent = histogram->GetBinContent(i+1);
        binSum += binContent;
        if (maxBin < binContent) maxBin = binContent;
    }
    Double_t ratio_hist = ( (double)(binSum) / (maxBin * bins) );
    Double_t integral_hist = scale_x * scale_y * ratio_hist;
    */
    return integral_points;
}


CsGenNeumann::CsGenNeumann() {

}

#include <string>
#include <iostream>
#include <vector>
#include <exception>
#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include "VGenerator.h"
#include "CsPrScalarMesonPhoton.h"
#include "CsPrTest.h"


Int_t VGenerator::FillTree(TTree* tree, UInt_t N) {
    const UInt_t dim = Process->NOut();
    TLorentzVector momenta[dim];
    for (size_t i = 0; i < dim; ++i) {
        tree->Branch( Process->OutParticle(i).Name(), &momenta[i] );
    }
    for (size_t i = 0; i < N; ++i) {
        MakeEvent(momenta);
        tree->Fill();
    }
    integral = Integral(); // TODO: change it to the virtual method "Finalize"?
    return 0;
}


Int_t VGenerator::SetProcess(TString prName) {
    processName = prName;
    if ( processName.EqualTo("scalar_meson_photon") )
        { Process = new CsPrScalarMesonPhoton(); return 0; }
    if ( processName.EqualTo("test") )
        { Process = new CsPrTest(); return 0; }
    std::cerr << "\tERROR! The process \"" << prName << "\" is invalid!"
              << std::endl;
    throw std::exception();
    return 1;
}


TLorentzVector* VGenerator::AnglesToMomenta( Double_t*       angles,
                                             TLorentzVector* momenta ) {

    const Double_t E = Process->BeamEnergy();       // Energy of the beam
    const Double_t M_out0 = Process->OutParticle(0).Mass();   // Mass of the meson a0
    const Double_t E_out0 = Process->OutParticle(0).Momentum().E();
    // TOASK: isn't it slow to initialize all these variables each time
    //        I call MakeEvent(momenta)?

    Double_t r     = TMath::Sqrt( E_out0*E_out0 - M_out0*M_out0 );
    Double_t theta = TMath::ACos(2 * angles[0] - 1);
    Double_t phi   = angles[1] * 2 * TMath::Pi();

    momenta[0].SetXYZM( r * TMath::Sin(theta) * TMath::Cos(phi)
                     ,  r * TMath::Sin(theta) * TMath::Sin(phi)
                     ,  r * TMath::Cos(theta)
                     ,  M_out0);
    momenta[1].SetXYZT( - momenta[0].X(),
                        - momenta[0].Y(),
                        - momenta[0].Z(),
                        2*E - momenta[0].T() );

    return momenta;
}

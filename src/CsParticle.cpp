#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include "CsParticle.h"

/*
CsParticle::CsParticle(Int_t pdg) {
    std::cout << "\tWarning! "
              << "CsParticle(Int_t pdg) is not imlemented."
              << std::cout;
}

CsParticle::CsParticle(Int_t pdg, TVector3 momentum) {
    std::cout << "\tWarning! "
              << "CsParticle(Int_t pdg, TVector3 momentum) is not imlemented."
              << std::cout;
}
*/
CsParticle::CsParticle(TString name, Double_t mass) {
    this->name = name;
    this->mass = mass;
    this->momentum.SetXYZM(0.0, 0.0, 0.0, mass);
}

CsParticle::CsParticle(TString name, Double_t mass, TLorentzVector momentum) {
    this->name = name;
    this->mass = mass;
    this->momentum = momentum;
}

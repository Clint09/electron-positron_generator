#include <iostream>
#include <exception>
#include "TTree.h"
#include "TMath.h"
#include "TLorentzVector.h"

#include "VProcess.h"
#include "CsPrScalarMesonPhoton.h"


Double_t CsPrScalarMesonPhoton::FormfactorSqrd(Double_t s) {
    //Double_t a = 0.011, b = 0.190, L = 350.0; // f0
    Double_t a = 0.0027, b = 0.090, L = 250.0; // a0
    Double_t s_caret = s / L / L;
    Double_t FF = (1 + a * s_caret) / (1 - b * s_caret + a * s_caret * s_caret);
    return FF * FF;
}

Double_t CsPrScalarMesonPhoton::PDF(Int_t nDim, Double_t* args) {
    /**
    * Differential cross section for a_0(980).
    * All the variables are in MeV.
    */
    if (this->E == 0) {
        std::cerr << "\tERROR! The energy is 0!" << std::endl;
        throw std::exception();
        return 0;
    }

    Double_t e_H = outParticles.at(0).Momentum().T();

    Double_t r     = TMath::Sqrt( e_H*e_H - m_H*m_H );   // Spherical coordinates
    Double_t theta = TMath::ACos(2 * args[0] - 1);       // of meson momentum
    Double_t phi   = args[1] * 2 * TMath::Pi();

    TLorentzVector  q( 0.0,  0.0,  0.0,  0.0 ),             // 4momentum of meson
                    p_minus = inParticles.at(0).Momentum(), // 4momentum of e-
                    p_plus = inParticles.at(1).Momentum();  // 4momentum of e+

    q.SetXYZM( r * TMath::Sin(theta) * TMath::Cos(phi)   // Px of meson's momentum
            ,  r * TMath::Sin(theta) * TMath::Sin(phi)   // Py of meson's momentum
            ,  r * TMath::Cos(theta)                     // Pz of meson's momentum
            ,  m_H);                                     // m of meson

    Double_t t = (p_minus - q) * (p_minus - q);             // Mandelstam t

    return PDFt(t);
}


Double_t CsPrScalarMesonPhoton::ReversedPDF(Int_t nDim, Double_t* args){return 0;}

Double_t CsPrScalarMesonPhoton::PDFt(Double_t t) {
    Double_t s = 4 * E * E;
    Double_t part1 = 4 * TMath::Pi() * alpha * Gamma / TMath::Power(m_H,3),
             part2 = FormfactorSqrd(s) / s,
             part3 = 1
                   + 2*t / s
                   - 2*m_H*m_H / s
                   + 2*t*t / (s*s)
                   - 2*m_H*m_H*t / (s*s)
                   + TMath::Power(m_H,4) / (s*s);

    return part1 * part2 * part3;
}

void CsPrScalarMesonPhoton::PrintInfo() {
    std::cout <<
        "Electron-positron (e+ e-) anihillation through the virtual photon (gamma*)\n\
        to scalar meson and photon (S gamma)."
    << std::endl;
}

Int_t CsPrScalarMesonPhoton::Initialize() {
    // TOASK: initialization for the second time. It is done for being possible
    //        to call SetBeamEnergy after SetProcess.
    Double_t p_e = TMath::Sqrt( E*E - m_e*m_e );      // Momentum of the e-
    Double_t e_H = E + (m_H*m_H) / (4 * E);           // Energy of meson

    // in-particles
    inParticles.clear();
    TLorentzVector p_electron(0.0,  0.0,  p_e,   E );
    TLorentzVector p_positron(0.0,  0.0, -p_e,   E );
    CsParticle electron("electron", m_e, p_electron);
    CsParticle positron("positron", m_e, p_positron);
    inParticles.push_back(electron);
    inParticles.push_back(positron);

    // out-particles
    outParticles.clear();
    TLorentzVector p_meson   (0.0,  0.0,  0.0,  e_H);
    TLorentzVector p_photon  (0.0,  0.0,  0.0,  2*E - e_H);
    CsParticle meson ("meson" , m_H, p_meson );
    CsParticle photon("photon", 0.0, p_photon);
    outParticles.push_back(meson);
    outParticles.push_back(photon);

    std::cout << "The process is initialized to scalar meson + photon." << std::endl
              << "E = " << E << " (in-particle energy)."
              << std::endl;

    Double_t p_H = TMath::Sqrt( e_H*e_H - m_H*m_H );
    t_limit[0] = TMath::Power( ( InParticle(0).Energy() - OutParticle(0).Energy() ), 2 )
               - TMath::Power( InParticle(0).Momentum().Z(), 2 )
               - 2 * InParticle(0).Momentum().Z() * p_H - p_H*p_H;
    t_limit[1] = TMath::Power( ( InParticle(0).Energy() - OutParticle(0).Energy() ), 2 )
               - TMath::Power( InParticle(0).Momentum().Z(), 2 )
               + 2 * InParticle(0).Momentum().Z() * p_H - p_H*p_H;
    major = TMath::Max( PDFt(t_limit[0]), PDFt(t_limit[1]) );
    // TODO: all these limits on t are common for all process
    // (cause it is the kinematics of 2->2)
    // you need to move it from the process class
    std::cout << "major = " << major
              << ", t_min = " << t_limit[0]
              << ", t_max = " << t_limit[1]
              << ", dsigma(t_min) = " << PDFt( t_limit[0] )
              << ", dsigma(t_max) = " << PDFt( t_limit[1] )
              << std::endl;

    return 0;
}

CsPrScalarMesonPhoton::CsPrScalarMesonPhoton() {
    // TODO: check one more time if this constructor is even necessary;
    Double_t p_e = TMath::Sqrt( E*E - m_e*m_e );      // Momentum of the e-
    Double_t e_H = E + (m_H*m_H) / (4 * E);           // Energy of meson

    // in-particles
    inParticles.clear();
    TLorentzVector p_electron(0.0,  0.0,  p_e,   E );
    TLorentzVector p_positron(0.0,  0.0, -p_e,   E );
    CsParticle electron("electron", m_e, p_electron);
    CsParticle positron("positron", m_e, p_positron);
    inParticles.push_back(electron);
    inParticles.push_back(positron);

    // out-particles
    outParticles.clear();
    TLorentzVector p_meson   (0.0,  0.0,  0.0,  e_H);
    TLorentzVector p_photon  (0.0,  0.0,  0.0,  2*E - e_H);
    CsParticle meson ("meson" , m_H, p_meson );
    CsParticle photon("photon", 0.0, p_photon);
    outParticles.push_back(meson);
    outParticles.push_back(photon);

}

#include <functional>
#include <iostream>
#include <exception>
#include "TRandom3.h"

#include "CsGenTFoam.h"


Int_t CsGenTFoam::Initialize() {

    if (Process == NULL) {
        throw std::exception();
        std::cerr << "\tERROR! The PROCESS is not set!"
                  << std::endl;
        return 1;
    }

    if (Process->InParticles().size() != 2 ||
        Process->OutParticles().size() != 2) {
        throw std::exception();
        std::cerr << "\tERROR! This generator works only with 2->2 process!"
                  << std::endl;
        return 1;
    }

    Process->Initialize();

    TRandom *PseRan = new TRandom3();
    PseRan->SetSeed(seed);

    Foam = new TFoam("foam");
    Foam->SetPseRan(PseRan);
    Foam->SetkDim(dim);
    Foam->SetnCells(nCells);
    Foam->SetRho(Process);
    Foam->Initialize();

    std::cout << "The generator is initialized." << std::endl;

    return 0;
}


Int_t CsGenTFoam::MakeEvent(TLorentzVector* momenta) {
    // TOASK: is it good for efficiency of the code to use
    //        MakeEvent(momenta) each event?
    // TODO: NOW IT WORKS ONLY FOR 2->2 PROCESS!
    if (Foam == NULL) {
        std::cerr << "\t ERROR! TFoam is not initialized!" << std::endl;
        throw std::exception();
        return 1;
    }
    Double_t spher[dim];   // theta, phi of the 1st particle
    Foam->MakeEvent();
    Foam->GetMCvect(spher);
    AnglesToMomenta(spher, momenta);

    return 0;
}


Double_t CsGenTFoam::Integral() {
    Double_t IntNorm, Errel;
    Foam->Finalize(IntNorm, Errel);
    //integral = IntNorm;
    return IntNorm;
}


CsGenTFoam::CsGenTFoam() {

}

CPP=g++
CFLAGS= -Wall -std=gnu++11             \
		$(shell root-config --cflags)  \
        $(shell clhep-config --include)
INC= -Iinclude -I/home/gecko/software/yaml-cpp/include
LIBS= $(shell root-config --libs)      \
	  $(shell clhep-config --libs)     \
	  -lFoam
MPI = mpic++

all: generate.out

generate.out: main.cpp
	$(CPP) $(CFLAGS) -o $@ $^ $(LIBS)

test.out: test.cpp \
		  src/*
	$(CPP) $(CFLAGS) $(INC) -o $@ $^ $(LIBS)

spectre.out: spectre.cpp \
		  src/*
	$(CPP) $(CFLAGS) $(INC) -o $@ $^ $(LIBS)

test_spectre.out: test_spectre.cpp \
			src/*
	$(CPP) $(CFLAGS) $(INC) -o $@ $^ $(LIBS)

mpi.out: mpi.cpp \
		  src/*
	$(MPI) $(CFLAGS) $(INC) -o  $@ $^ $(LIBS)


static_mpi.out: mpi.cpp \
		  src/*
	$(MPI) $(CFLAGS) $(INC) -o  $@ $^ $(LIBS)

clean:
	rm -f *.out *.root *.png *.ps *.csv

.PHONY: all clean

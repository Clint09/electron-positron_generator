#include <iostream>
#include <vector>
#include <fstream>
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"

#include "VProcess.h"
#include "VGenerator.h"
#include "CsGenTFoam.h"
#include "CsGenNeumann.h"
#include "CsPrScalarMesonPhoton.h"


int main(int argc, char** argv) {



/*
    const int N = 1;
    float ints[N];
    //TFile file("tree.root","RECREATE","4momenta");
    for ( int i = 0; i < N; i++) {
        int E = 1000 + 1000* i;
        //Gen->SetBeamEnergy(E);
        //std::cout <<"The beam energy is set to " << E
    //              << std::endl;
    TTree *tree = new TTree("the_tree", "it is a tree");
    std::cout << "Trying to fill the TTree" << std::endl;
    Gen->FillTree(tree, 200000);
    std::cout << std::endl;
    tree->Print();
    //file.Write();
    //file.Close();

    ints[i] = Gen->GetIntegral();
    std::cout << "The integral is " << ints[i] << std::endl;
    std::cout << std::endl;
}

    for( int i = 0; i < N; i++ ){
        std::cout << "E:" << 1000+1000*i << " I: " << ints[i] << std::endl;
    }
*/
    int nEvents = 1e7;
    if (argc > 1) nEvents = std::stoi(argv[1]);

    double e1 = 500;
    double e2 = 1500;
    double de = 25;

    std::vector<double> vect_e;
    std::vector<double> vect_neum;
    std::vector<double> vect_foam;

    for( double energy = e1; energy <= e2; energy+=de) {

        vect_e.push_back(energy);


        std::cout << "\n\tTFoam method, E = " << energy
                  << std::endl;
        VGenerator *GenTFoam = new CsGenTFoam();
        GenTFoam->SetProcess("scalar_meson_photon");
        GenTFoam->SetBeamEnergy(energy);
        GenTFoam->Initialize();

        TFile fileTFoam("treeTFoam.root","RECREATE","4momenta");
        TTree *treeTFoam = new TTree("TFoam_tree", "TFoam");
        std::cout << "Trying to fill the TTree" << std::endl;
        GenTFoam->FillTree(treeTFoam, nEvents);
        std::cout << std::endl;
        treeTFoam->Print();
        fileTFoam.Write();
        fileTFoam.Close();
        Double_t integralTFoam = GenTFoam->GetIntegral();
        std::cout << "The output file is treeTFoam.root."
                  << std::endl;
        std::cout << "The integral is " << integralTFoam << std::endl;
        std::cout << std::endl;
        vect_foam.push_back(integralTFoam);


        std::cout << "\n\tNeumann method, E = " << energy
                  << std::endl;
        VGenerator *GenNeum = new CsGenNeumann();
        GenNeum->SetProcess("scalar_meson_photon");
        GenNeum->SetBeamEnergy(energy);
        GenNeum->Initialize();

        TFile fileNeum("treeNeum.root","RECREATE","4momenta");
        TTree *treeNeum = new TTree("Neum_tree", "Neumann");
        std::cout << "Trying to fill the TTree" << std::endl;
        GenNeum->FillTree(treeNeum, nEvents);
        std::cout << std::endl;
        treeNeum->Print();
        fileNeum.Write();
        fileNeum.Close();
        Double_t integralNeum = GenNeum->GetIntegral();
        std::cout << "The output file is treeNeum.root."
                  << std::endl;
        std::cout << "The integral is " << integralNeum << std::endl;
        std::cout << std::endl;
        vect_neum.push_back(integralNeum);
    }

    std::ofstream outfile;
    outfile.open("integrals.csv");
    std::cout << "Energy \t FOAM \t Neumann" << std::endl;
    outfile << "Energy \t FOAM \t Neumann" << std::endl;
    for(std::vector<int>::size_type i = 0; i != vect_e.size(); ++i) {
        std::cout << vect_e.at(i) << " "
                  << vect_foam.at(i) << " "
                  << vect_neum.at(i) << std::endl;
        outfile << vect_e.at(i) << " "
                << vect_foam.at(i) << " "
                << vect_neum.at(i) << std::endl;
    }
    outfile.close();
    return 0;
}

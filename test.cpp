#include <iostream>
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"

#include "VProcess.h"
#include "VGenerator.h"
#include "CsGenTFoam.h"
#include "CsGenNeumann.h"
#include "CsPrScalarMesonPhoton.h"


int main(int argc, char** argv) {

    std::cout << "\n\tTEST OF CLASSES INITIALIZATION HAS BEEN STARTED.\n"
              << std::endl;

    //Test of the class << CsPrScalarMesonPhoton >>
    std::cout << "\n\tTest of the class << CsPrScalarMesonPhoton >>."
              << std::endl;

    VProcess *Pr = new CsPrScalarMesonPhoton();
    std::cout << "Process of e+ e- -> S photon is created."
              << std::endl;

    const Int_t nDim = 2;
    Double_t args[nDim] = {1, 1};
    std::cout << "PDF of coordinates (1, 1) equals " << Pr->PDF(nDim, args)
              << std::endl;

    Double_t E = 2000;
    std::cout << "Setting beam energy to " << E << "."
              << std::endl;
    Pr->SetBeamEnergy(E);
    std::cout << "PDF of coordinates (1, 1) equals " << Pr->PDF(nDim, args)
              << std::endl;
    std::cout << std::endl;


    //Test of the class <<CsParticle>>.
    std::cout <<"\n\tTest of the class <<CsParticle>>." << std::endl;

    CsParticle *Part = new CsParticle();
    std::cout <<"CsParticle with the empty arguments is created." << std::endl
              << "The name is " << Part->Name() << ", the mass is " << Part->Mass()
              << std::endl;

    CsParticle *Par = new CsParticle("alpha'", 1000.0);
    std::cout <<"CsParticle with arguments (alpha' and 1000) is created." << std::endl
              << "The name is " << Par->Name() << ", the mass is " << Par->Mass()
              << std::endl;
    std::cout << std::endl;


    // Test of the class <<VGenerator>> and
    std::cout <<"\n\tTest of the class <<CsGenTFoam>>"
              << std::endl;

    VGenerator *Gen = new CsGenTFoam();
    std::cout <<"CsGenTFoam with the empty arguments is created."
              << std::endl;

    Gen->SetProcess("scalar_meson_photon");
    std::cout <<"The process is set to CsPrScalarMesonPhoton"
              << std::endl;
    std::cout <<"The energy of the Process is " << Gen->GetProcessEnergy()
              << std::endl;
    // TOASK:: SetProcess MUST be before SetBeamEnergy. Is it bad?
    Double_t energy = 11000.0;
    Gen->SetBeamEnergy(energy);
    std::cout <<"The beam energy is set to " << energy
              << std::endl;
    std::cout <<"The energy of the Process is " << Gen->GetProcessEnergy()
              << std::endl;

    Gen->Initialize();
    std::cout <<"CsGenTFoam is initialized."
              << std::endl;

    TLorentzVector momenta[2];
    Gen->MakeEvent(momenta);
    std::cout << "The generated event is" << std::endl;
    for ( int i = 0; i < 2; ++i ) {
        std::cout << "PX " << momenta[i].X() << std::endl;
        std::cout << "PY " << momenta[i].Y() << std::endl;
        std::cout << "PZ " << momenta[i].Z() << std::endl;
        std::cout << "E  " << momenta[i].T() << std::endl;
    }
    std::cout << std::endl;

    std::cout << "\n\tTEST OF CLASSES INITIALIZATION HAS FINISHED.\n"
              << std::endl;

    std::cout << "\n\tTEST OF EVENT GENERATION HAS BEEN STARTED.\n"
              << std::endl;


/*
    const int N = 1;
    float ints[N];
    //TFile file("tree.root","RECREATE","4momenta");
    for ( int i = 0; i < N; i++) {
        int E = 1000 + 1000* i;
        //Gen->SetBeamEnergy(E);
        //std::cout <<"The beam energy is set to " << E
    //              << std::endl;
    TTree *tree = new TTree("the_tree", "it is a tree");
    std::cout << "Trying to fill the TTree" << std::endl;
    Gen->FillTree(tree, 200000);
    std::cout << std::endl;
    tree->Print();
    //file.Write();
    //file.Close();

    ints[i] = Gen->GetIntegral();
    std::cout << "The integral is " << ints[i] << std::endl;
    std::cout << std::endl;
}

    for( int i = 0; i < N; i++ ){
        std::cout << "E:" << 1000+1000*i << " I: " << ints[i] << std::endl;
    }
*/

    std::cout << "\n\tTesting TFoam method.\n"
              << std::endl;

    int nEvents = 1e7;
    energy = 1.3e3;
    if (argc > 1) nEvents = std::stoi(argv[1]);
    if (argc > 2) energy = std::stoi(argv[2]);

    VGenerator *GenTFoam = new CsGenTFoam();
    GenTFoam->SetProcess("scalar_meson_photon");
    GenTFoam->SetBeamEnergy(energy);
    GenTFoam->Initialize();

    TFile fileTFoam("treeTFoam.root","RECREATE","4momenta");
    TTree *treeTFoam = new TTree("TFoam_tree", "TFoam");
    std::cout << "Trying to fill the TTree" << std::endl;
    GenTFoam->FillTree(treeTFoam, nEvents);
    std::cout << std::endl;
    treeTFoam->Print();
    fileTFoam.Write();
    fileTFoam.Close();
    Double_t integralTFoam = GenTFoam->GetIntegral();
    std::cout << "The output file is treeTFoam.root."
              << std::endl;
    std::cout << "The integral is " << integralTFoam << std::endl;
    std::cout << std::endl;



    std::cout << "\n\tTesting Neumann method.\n"
              << std::endl;

    VGenerator *GenNeum = new CsGenNeumann();
    GenNeum->SetProcess("scalar_meson_photon");
    GenNeum->SetBeamEnergy(energy);
    GenNeum->Initialize();

    TFile fileNeum("treeNeum.root","RECREATE","4momenta");
    TTree *treeNeum = new TTree("Neum_tree", "Neumann");
    std::cout << "Trying to fill the TTree" << std::endl;
    GenNeum->FillTree(treeNeum, nEvents);
    std::cout << std::endl;
    treeNeum->Print();
    fileNeum.Write();
    fileNeum.Close();
    Double_t integralNeum = GenNeum->GetIntegral();
    std::cout << "The output file is treeNeum.root."
              << std::endl;
    std::cout << "The integral is " << integralNeum << std::endl;
    std::cout << std::endl;

    return 0;
}

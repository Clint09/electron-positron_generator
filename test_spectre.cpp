#include <iostream>
#include <vector>
#include <fstream>
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"

#include "VProcess.h"
#include "VGenerator.h"
#include "CsGenTFoam.h"
#include "CsGenNeumann.h"
#include "CsPrScalarMesonPhoton.h"


int main(int argc, char** argv) {

    int nEvents = 1e6;
    if (argc > 1) nEvents = std::stoi(argv[1]);

    double e1 = 500;
    double e2 = 1500;
    double de = 25;

    std::vector<double> vect_e;
    std::vector<double> vect_neum;

    for( double energy = e1; energy <= e2; energy+=de) {

        vect_e.push_back(energy);

        VGenerator *GenNeum = new CsGenNeumann();
        GenNeum->SetProcess("test");
        GenNeum->SetBeamEnergy(energy);
        GenNeum->Initialize();

        TFile fileNeum("treeNeum.root","RECREATE","4momenta");
        TTree *treeNeum = new TTree("Neum_tree", "Neumann");
        std::cout << "Trying to fill the TTree" << std::endl;
        GenNeum->FillTree(treeNeum, nEvents);
        std::cout << std::endl;
        treeNeum->Print();
        fileNeum.Write();
        fileNeum.Close();
        Double_t integralNeum = GenNeum->GetIntegral();
        std::cout << "The output file is treeNeum.root."
                  << std::endl;
        std::cout << "The integral is " << integralNeum << std::endl;
        std::cout << std::endl;
        vect_neum.push_back(integralNeum);
    }

    std::ofstream outfile;
    outfile.open("integrals.csv");
    std::cout << "Energy \t Neumann" << std::endl;
    outfile << "Energy \t Neumann" << std::endl;
    for(std::vector<int>::size_type i = 0; i != vect_e.size(); ++i) {
        std::cout << vect_e.at(i) << " "
                  << vect_neum.at(i) << std::endl;
        outfile << vect_e.at(i) << " "
                << vect_neum.at(i) << std::endl;
    }
    outfile.close();
    return 0;
}

#include <iostream>
#include <vector>
#include "mpi.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TLorentzVector.h"

#include "VProcess.h"
#include "VGenerator.h"
#include "CsGenTFoam.h"
#include "CsGenNeumann.h"
#include "CsPrScalarMesonPhoton.h"


int main(int argc, char** argv) {

    int root = 0, tag = 0;
    int rank, commSize;

    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSize);
    MPI_Status status;

    Int_t N = 1e7;
    Int_t partSize = N/commSize;
    Int_t shift = N%commSize;

    const Int_t dim = 2;

    if (rank == root) {
        TString filename = "tree.root";
        TFile file(filename, "RECREATE", "4momenta");
        TTree *tree = new TTree("the_tree", "it is a tree");

        TLorentzVector m[dim];
        TString branch_names[2] = {"meson", "photon"};
        for (size_t i = 0; i < dim; ++i) {
            tree->Branch( branch_names[i], &m[i] );
        }

        VGenerator *Gen = new CsGenTFoam();
        Gen->SetProcess("scalar_meson_photon");
        Gen->Initialize();

        //here should be one more loop over events
        Double_t mom[8];
        for(int j = 0; j < partSize + shift; ++j){
            // generating events
            Gen->MakeEvent(m);
            tree->Fill();

            for (int i = root+1; i < commSize; ++i) {
                if (N==1)
                    std::cout << rank << ") Waiting for the event from " << i << std::endl;
                MPI_Recv(&mom, 8, MPI_DOUBLE, i, tag, MPI_COMM_WORLD, &status);
                TLorentzVector m[dim];
                m[0].SetXYZT(mom[0], mom[1], mom[2], mom[3]);
                m[1].SetXYZT(mom[4], mom[5], mom[6], mom[7]);
                tree->Fill();

            }
        }

        tree->Print();
        file.Write();
        file.Close();
    }

    else {
        VGenerator *Gen = new CsGenTFoam();
        Gen->SetProcess("scalar_meson_photon");
        Gen->Initialize();

        TLorentzVector m[dim];

        //here should be one more loop over events
        for(int j = 0; j < partSize; ++j) {
            Gen->MakeEvent(m);
            if (N==1)
                std::cout << rank << ") Event has been made" << std::endl;
            Double_t mom[8] = {
                m[0].X(), m[0].Y(), m[0].Z(), m[0].T(),
                m[1].X(), m[1].Y(), m[1].Z(), m[1].T() };
            MPI_Send(mom, 8, MPI_DOUBLE, root, tag,  MPI_COMM_WORLD);
            if (N==1) {
                std::cout << rank << ") Event has been send" << std::endl <<
                m[0].X() << " " << m[0].Y() << " " << m[0].Z() << " " << m[0].T() << std::endl <<
                m[1].X() << " " << m[1].Y() << " " << m[1].Z() << " " << m[1].T() << std::endl;
            }
        }
    }

    MPI_Finalize();

    return 0;
}
